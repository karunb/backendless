//
//  School.swift
//  NorthwestKCProgrammingApp
//
//  Created by Bourishetty,Karun on 4/5/19.
//  Copyright © 2019 Bourishetty,Karun. All rights reserved.
//

@objcMembers

class School: NSObject{
    
    
    
    var name: String?
    
    var coach: String
    
    var teams: [Team]
    
    
    
    override var description: String { // NSObject adheres to the CustomStringConvertible protocol
        
        return "Name: \(name ?? ""), Coach: \(coach), ObjectId: \(objectId ?? "N/A")"
        
    }
    
    
    
    var objectId:String?
    
    
    
    func addTeam(name: String, students: [String]){
        
        teams.append(Team(name: name, students: students))
        
    }
    
    
    
    init(name: String, coach: String, teams: [Team]) {
        
        self.name = name
        
        self.coach = coach
        
        self.teams = teams
        
    }
    
    
    
    convenience override init(){
        
        self.init(name: "", coach: "", teams: [])
        
    }
    
    
    
    static func == (lhs: School, rhs: School) -> Bool {
        
        return lhs.name == rhs.name && lhs.coach == rhs.coach && lhs.teams == rhs.teams
        
    }
    
}
