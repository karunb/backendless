//
//  Team.swift
//  NorthwestKCProgrammingApp
//
//  Created by Bourishetty,Karun on 4/5/19.
//  Copyright © 2019 Bourishetty,Karun. All rights reserved.
//

@objcMembers

class Team : NSObject{
    
    
    
    var name: String?
    
    var students: [String]
    
    
    
    override var description: String { // NSObject adheres to the CustomStringConvertible protocol
        
        return "Name: \(name ?? ""), Students: \(students)"
        
    }
    
    var objectId:String?
    
    
    
    init(name: String, students: [String]){
        
        self.name = name
        
        self.students = students
        
    }
    
    
    
    convenience override init(){
        
        self.init(name: "", students: [])
        
    }
    
    
    
    static func == (lhs: Team, rhs: Team) -> Bool {
        
        return lhs.name == rhs.name && lhs.students == rhs.students
        
    }
    
}
